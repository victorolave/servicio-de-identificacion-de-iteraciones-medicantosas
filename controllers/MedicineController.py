import json
import os
from flask import jsonify
from models.Medicine import Medicine

class MedicineController:

    def __init__(self, path):
        self.path = path
        self.medicines = []
        self.read_file()

    def read_file(self):
        if not os.path.isfile('../database/data.json'):
            with open(self.path) as file:
                data = json.load(file)
                medicines = []
                for medicine in data['medicines']:
                    tmp = Medicine(
                        medicine['name'],
                        medicine['not_consume_with']
                    )

                    medicines.append(tmp)

                self.medicines = medicines

        else:
            self.write_file()
            self.read_file()

    def write_file(self):

        data = {}
        data['medicines'] = []

        if len(self.medicines) > 0:
            for medicine in self.medicines:
                tmp = medicine.toDictionary()
                data['medicines'].append(tmp)

        with open(self.path, 'w') as file:
            json.dump(data, file, indent=2)

    def get_medicines(self):
        return self.medicines

    def get_medicines_json(self):

        data = {}
        data['medicines'] = []

        if len(self.medicines) > 0:
            for medicine in self.medicines:
                tmp = medicine.toDictionary()
                data['medicines'].append(tmp)

            return jsonify(data)

        else:
            raise Exception('No records found.')

    def exist(self, name):
        for medicine in self.medicines:
            if medicine.name == name:
                return True

        return False

    def add_medicine(self, name, not_consume_with=[], recursion=False):

        if not self.exist(name):
            if len(not_consume_with) > 0:
                if not recursion:
                    for medicine in not_consume_with:
                        if not self.exist(medicine):
                            self.add_medicine(medicine, [name], True)

                new_medicine = Medicine(
                    name, not_consume_with
                )

                self.medicines.append(new_medicine)

            else:
                new_medicine = Medicine(name)
                self.medicines.append(new_medicine)

            self.write_file()

        else:
            raise Exception('The medicine is alredy registered')

    def identify_medicines(self, medicine_a, medicine_b):

        if self.exist(medicine_a) and self.exist(medicine_b):
            self.iterations(medicine_a, medicine_b)
            raise Exception('Iteration identification already saved.')

        elif self.exist(medicine_a):
            self.add_medicine(medicine_b, [medicine_a])
            self.iterations(medicine_a, medicine_b)

        elif self.exist(medicine_b):
            self.add_medicine(medicine_a, [medicine_b])
            self.iterations(medicine_a, medicine_b)

        else:
            self.add_medicine(medicine_a, [medicine_b])

    def iterations(self, medicine_a, medicine_b):

        if self.exist(medicine_a) and self.exist(medicine_b):

            ma = self.get_medicine(medicine_a)
            mb = self.get_medicine(medicine_b)

            for n in ma.not_consume_with:
                if n == mb.name:
                    ma = True

            if type(ma) is Medicine:
                ma.addMedicine(mb.name)
                self.write_file()

            for n in mb.not_consume_with:
                if n == ma.name:
                    mb = True

            if type(mb) is Medicine:
                mb.addMedicine(ma.name)
                self.write_file()



    def get_medicine(self, medicine):
        for m in self.medicines:
            if medicine == m.name:
                return m
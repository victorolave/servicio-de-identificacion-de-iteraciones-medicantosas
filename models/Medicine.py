from flask import jsonify


class Medicine:

    def __init__(self, name, not_consume_with=[]):
        self.name = name
        self.not_consume_with = not_consume_with

    def addMedicine(self, medicine):
        if medicine not in self.not_consume_with:
            self.not_consume_with.append(medicine)
            return "Successfully Registered Medicine"
        else:
            return "This medicine was already registered."

    def toDictionary(self):
        return {
            'name': self.name,
            'not_consume_with': self.not_consume_with
        }

    def toJson(self):
        return jsonify(self.toDictionary())

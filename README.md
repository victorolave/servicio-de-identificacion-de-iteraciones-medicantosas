<h1>Servicio para la Identificación de Iteraciones Medicamentosas</h1>

<h2>Carpetas</h2>

- **_controllers:_** En esta carpeta se encuentran la clase que se encarga de controlar los datos y sus interacciones.
- **_database:_** En esta carpeta se encuentra el archivo json donde se esta almacenando la información.
- **_models:_** En esta carpeta se encuentran los objetos que se utilizan en el sistema.

<h2>Endpoints</h2>

| Ruta | Metodo | Petición |  Descripción |
| ------ | ------ | ------ | ------ | 
| /getMedicines | GET | none | Esta ruta le permite acceder a todos los registros existentes en el archivo json.
| /addMedicine | POST | `{ "name": "acetaminofem", "not_consume_with": ["dolex", "rinolin"] }` | Esta ruta le permite añadir un medicamento y sus respectivos medicamentos con los cuales no se debe combinar.
| /identifyMed | POST | `{ "medicine_a": "advil", "medicine_b": "dolex" }` | Este metodo le permite agregar de manera directa la iteracion entre dos medicamentos.
| /getMedicine/:name | GET | none | Este metodo le permite obtener el medicamento a partir del nombre de este.

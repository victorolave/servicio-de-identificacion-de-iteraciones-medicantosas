import os
import json
from controllers.MedicineController import MedicineController
from flask import Flask, request, jsonify

app = Flask(__name__)

controller = MedicineController('database/data.json')


@app.route('/getMedicines')
def get_medicines():
    try:
        return controller.get_medicines_json()
    except Exception as ex:
        return jsonify({
            "message": "Server Error",
            "error": str(ex)
        })


@app.route('/addMedicine', methods=['POST'])
def add_medicines():
    data = request.json

    try:
        controller.add_medicine(data['name'],data['not_consume_with'])

        return jsonify({
            "message": "Medicine Added",
            "medicine": {
                "name": data['name']
            }
        })

    except Exception as e:
        return jsonify({
            "message": "Server Error",
            "error": str(e)
        })


@app.route('/identifyMed', methods=['POST'])
def identify_med():
    data = request.json

    try:
        controller.identify_medicines(data['medicine_a'], data['medicine_b'])

        return jsonify({
            "message": 'Iteration identification saved.'
        })
    except Exception as e:
        return jsonify({
            'message': 'Server Error',
            'error': str(e)
        })


@app.route('/getMedicine/<name>')
def get_medicine(name):
    medicine = controller.get_medicine(name)
    return medicine.toJson()

@app.route('/')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    app.run()
